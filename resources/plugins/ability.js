import Vue from 'vue'

const { createAbilityForUser } = require('../../lib/ability')

export const $can = (user, ...args) => {
  const userAbility = createAbilityForUser(user)

  return userAbility.can(...args)
}

Vue.mixin({
  methods: {
    $can,
  },
})
