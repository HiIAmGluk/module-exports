import { $can } from '../../plugins/ability'

export const getters = {
  canCreateProject() {
    const currentProject = { id: 1 }
    const user = { email: 'foo@bar.com' }

    return $can(user, 'create', currentProject)
  },
}

