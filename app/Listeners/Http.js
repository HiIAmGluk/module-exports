const Http = {}

/**
 * listener for Http.start event, emitted after
 * starting http server.
 */
Http.onStart = function () {
  const Request = use('Adonis/Src/Request')
  const { createAbilityForUser } = require('../../lib/ability')

  Request.macro('userAbility', user => createAbilityForUser(user))
}

module.exports = Http
