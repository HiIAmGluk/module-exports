const { mapUserWorkspaces } = require('./utils')

function createAbilityForUser() {
  return {
    can() {
      return true
    },
  }
}

module.exports = { createAbilityForUser }
